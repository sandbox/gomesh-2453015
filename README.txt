README.txt
==========
The purpose of this module is to secure drupal backend by overriding node/add path.
It's will rename path like '/node/add/ ' to '/something/...'. This module can be effective against create basic content  spam 
bots or malicious .

Installation
------------
1. Install this module like any other Drupal module (place it in the
modules directory for your site and enable it on the `admin/build/modules` page.
2. Go to admin/config/user-interface/alter-main-path and configure how to 
override node/add paths.

